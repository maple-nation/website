import React from 'react';
import { Switch, Route } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from 'components/Header';
import Footer from 'components/Footer';
import Loading from 'components/Loading';

import * as actions from 'state/actions';
import { mainNavigation } from 'state/selectors';
import { FULFILLED, IDLE } from 'state/constants';

// containers
import { containers } from 'config';
import PageContainer from 'containers/PageContainer';

import styled from 'react-emotion';
import { copy } from 'stylesheet';


const styles = {
  padding: '0 1.25rem',
  ...copy,
};

const {
  Page,
  Welcome,
} = containers;

const App = props => {
  const { actions, className, mainNavigation, status } = props;

  if (status.fetchPages === IDLE) {
    actions.fetchPages();
  }

  return (
    <div className={className}>
      <Header pages={mainNavigation} />
      {status.fetchPages === FULFILLED ? (
        <Switch>
          <Route
            exact
            path={Page.path}
            component={PageContainer}
          />
          <Route
            exact
            path={Welcome.path}
            component={PageContainer}
          />
        </Switch>
      ) : (
        <Loading />
      )}
      <Footer />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    ...state,
    mainNavigation: mainNavigation(state),
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(
      {
        ...actions,
      },
      dispatch,
    ),
  };
}

export { App };
export default styled(
  connect(mapStateToProps, mapDispatchToProps)(App)
)(styles);
