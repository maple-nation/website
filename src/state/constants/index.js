export const IDLE = "IDLE";
export const PENDING = "PENDING";
export const FULFILLED = "FULFILLED";
export const REJECTED = "REJECTED";

export const FETCH_PAGES = "FETCH_PAGES";
