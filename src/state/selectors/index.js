import { createSelector } from 'reselect';
import find from "lodash.find";

const HOMEPAGE_ID = '4SqmDL1I0gkocUC2ucwMqm';

const pagesSelector = state => state.pages;
const paramsSelector = (state, props) => props.match.params || {};

const pageSlugSelector = createSelector(
  paramsSelector,
  params => params.page_slug || null
)

export const mainNavigation = createSelector(
  pagesSelector,
  pages => pages ? pages.filter(page => page.fields.mainNavigation === true) : [],
);

export const homepage = createSelector(
  pagesSelector,
  pages => pages ? find(pages, page => page.sys.id === HOMEPAGE_ID) : null,
);

export const selectedPage = createSelector(
  [pagesSelector, pageSlugSelector, homepage],
  (pages, pageSlug, home) => {
    if (!pageSlug) {
      return home;
    }

    return find(pages, page => page.fields.slug === pageSlug)
  }
)
