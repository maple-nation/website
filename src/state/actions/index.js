import { createClient } from 'contentful';
import { FETCH_PAGES } from 'state/constants';

const CONTENTFUL_SPACE_ID = process.env.REACT_APP_CONTENTFUL_SPACE_ID;
const CONTENTFUL_ACCESS_TOKEN = process.env.REACT_APP_CONTENTFUL_ACCESS_TOKEN;

const client = createClient({
  space: CONTENTFUL_SPACE_ID,
  accessToken: CONTENTFUL_ACCESS_TOKEN,
});

export const fetchPages = () => {
  const payload = client.getEntries({'content_type': 'page', order: 'fields.priority'}).then(({ items }) => items);
  return {
    type: FETCH_PAGES,
    payload,
  };
}
