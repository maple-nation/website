import {
  IDLE,
  PENDING,
  FULFILLED,
  REJECTED,
  FETCH_PAGES,
} from "state/constants";

const initialState = {
  fetchPages: IDLE,
};

export default (state = initialState, action) => {
  switch (action.type) {
    // fetchPages
    case `${FETCH_PAGES}_${PENDING}`:
      return { ...state, fetchPages: PENDING };
    case `${FETCH_PAGES}_${FULFILLED}`:
      return { ...state, fetchPages: FULFILLED };
    case `${FETCH_PAGES}_${REJECTED}`:
      return { ...state, fetchPages: REJECTED };

    default:
      return state;
  }
};
