import { combineReducers } from "redux";
import pages from './pages';
import status from './status';

export default combineReducers({
  pages,
  status,
});
