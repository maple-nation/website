import { FETCH_PAGES, FULFILLED } from 'state/constants';

const INITIAL_STATE = null;

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
  case `${FETCH_PAGES}_${FULFILLED}`:
    return action.payload;
  default:
    return state;
  }
}
