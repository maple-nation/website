import React, { Component } from 'react';
import { containers } from 'config';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from 'state/actions';
import Container from 'components/Container';
import Loading from 'components/Loading';
import PageView from 'views/PageView';
import { selectedPage } from "state/selectors";

class PageContainer extends Component {
  render = () => {
    const { page } = this.props;

    return (
      <Container title={page ? page.fields.name : containers.Page.title}>
        { page ? <PageView {...this.props} {...page} /> : <Loading /> }
      </Container>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    ...state,
    page: selectedPage(state, props),
  };
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        ...actions,
      },
      dispatch,
    ),
  };
}

export { PageContainer };
export default connect(mapStateToProps, mapDispatchToProps)(PageContainer);
