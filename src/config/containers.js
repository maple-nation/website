import parameterize from 'parameterize';

const createContainerConfig = (title, label=null, slug=null, path=null) => {
  const generatedLabel = label ? label : title;
  const generatedSlug = slug ? slug : parameterize(generatedLabel);
  const generatedPath = path ? path : `/${generatedSlug}`;

  return {
    label: generatedLabel,
    path: generatedPath,
    slug: generatedSlug,
    title,
  }

};

export default {
  Welcome: createContainerConfig('Welcome', 'Welcome', 'welcome', '/'),
  Page: createContainerConfig('Page', 'Page', 'page', '/:page_slug'),
};
