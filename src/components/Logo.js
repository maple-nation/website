import React from 'react';

const Logo = () => {
  return (
    <img src={'/assets/images/logo.png'} alt={'Maple Nation Logo'} />
  );
}

export default Logo;
