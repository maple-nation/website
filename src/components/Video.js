import React from 'react';
import ReactPlayer from 'react-player';
import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s, copy } from 'stylesheet';

const styles = css(s.flex, s.flexColumn, s.alignCenter, {
  marginBottom: '1.5625rem',

  '.media': css(s.relative, {
    marginBottom: '0.3125rem',
  }),

  '.playerContainer': css(s.relative, {
    overflow: 'hidden',
  }),

  '.player': css(s.absolute, s.t0, s.l0, {
    width: '100%',
    height: '100%',
  }),

  '.title': {
    marginBottom: '1.25rem',
  },

  '.description': {
    width: '85%',
    marginBottom: '1.5625rem',
  },

  '.text': css(s.brown, {
    ...copy.oswald,
  }),
});

const Video = (props) => {
  return (
    <div className={props.className}>
      <div className="media">
        <div className="playerContainer">
          <svg viewBox="0 0 16 9" width="100%" />
          <div className="player">
            <ReactPlayer
              url={props.fields.videoUrl}
              width='100%'
              height='100%'
            />
          </div>
        </div>
      </div>
      <div className="title">
        <h2>{props.fields.name}</h2>
      </div>
      <div className="description">
        <p className="text">{props.fields.description}</p>
      </div>
    </div>
  );
}

export default styled(Video)(styles);
