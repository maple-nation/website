import React from 'react';
import SponsorList from 'components/SponsorList';

import { styles as s, colors } from 'stylesheet';
import { css } from 'emotion';
import styled from 'react-emotion';

const styles = css(s.flex, s.flexColumn, s.alignCenter, {
  width: '100%',
  padding: '1.25rem',
  borderTop: `1px solid ${colors.red}`,

  '.copyright': {
    fontSize: '0.625rem',
    textAlign: 'center',
  }
});

const Footer = ({ className }) => {
  const currentYear = (new Date()).getFullYear();

  return (
    <footer className={className}>
      <SponsorList />
      <p className="copyright">{`© Center for Native Peoples and the Environment, SUNY College of Environmental Science and Forestry, ${currentYear}`}</p>
    </footer>
  );
}

export default styled(Footer)(styles);
