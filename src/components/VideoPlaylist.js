import React from 'react';
import map from 'lodash.map';
import Video from 'components/Video';

const VideoPlaylist = (props) => {
  return (
    <div>
      {map(props.videos, (video) => <Video key={video.sys.id} {...video} />)}
    </div>
  );
}

export default VideoPlaylist;
