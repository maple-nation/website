import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from 'components/Logo';
import Navigation from 'components/Navigation';

import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s, colors } from 'stylesheet';

const styles = css(s.flex, s.flexColumn, s.alignCenter, {
  width: '100%',
  padding: '1.25rem',
  borderBottom: `1px solid ${colors.red}`,

  ".logo": {
    width: '50%',
    marginBottom: '1.25rem',
  }
});

const Header = ({ className, pages }) => {
  return (
    <header className={className}>
      <NavLink className="logo" to={'/'}><Logo /></NavLink>
      <Navigation pages={pages} />
    </header>
  );
}

export default styled(Header)(styles);
