import React from 'react';
import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s } from 'stylesheet';

const styles = css(s.flex, s.alignCenter, s.justifyCenter, {
  minHeight: '100vh',
});

const Loading = ({ className }) => <div className={className}><p>{'Loading...'}</p></div>;

export default styled(Loading)(styles);
