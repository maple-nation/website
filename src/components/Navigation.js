import React from 'react';
import { NavLink } from 'react-router-dom';
import map from 'lodash.map';
import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s, breakpoints, copy } from 'stylesheet';

const styles = css(s.flex, s.bgYellow, s.flexRow, {
  '.linkContainer': css(s.relative, {

    '.subnav': css(s.absolute, s.l0, s.r0, s.bgWhite, {
      display: 'none',
      textAlign: 'center',
      padding: '0 0.625rem 0.625rem',
    }),

    ':hover': {
      [breakpoints.desktop]: {

        '.subnav': css(s.block),
      },
    },
  }),

  '.link': css(s.black, {
    padding: '0 0.625rem',
    textTransform: 'uppercase',
    textDecoration: 'none',
    fontSize: '0.75rem',
    ...copy.oswald,

    ':hover': css(s.red),

    [breakpoints.desktop]: {
      fontSize: '1.125rem',
    }
  }),

  '.subNavLink': css(s.block, s.red, {
    marginTop: '0.625rem',
    fontSize: '0.75rem',
    textDecoration: 'none',
    lineHeight: '1.2em',
    ...copy.lato,

    ':hover': css(s.brown),
  }),
});

const Navigation = (props) => {
  const renderSubNav = (pages) => {
    return (
      <div className="subnav">
        {map(pages, (page) => {
          return (
            <div key={page.sys.id}>
              <NavLink className="subNavLink" to={`/${page.fields.slug}`}>
                {page.fields.name}
              </NavLink>
            </div>
          );
        })}
      </div>
    );
  }

  const renderLinks = () => {
    return map(props.pages, (page) => {
      return (
        <div key={page.sys.id} className="linkContainer">
          <NavLink className="link" to={`/${page.fields.slug}`}>
            {page.fields.name}
          </NavLink>
          {page.fields.subpages && renderSubNav(page.fields.subpages)}
        </div>
      );
    });
  }

  return (
    <nav className={props.className}>
      <div className="linkContainer">
        <NavLink className="link" to={'/'}>{'Home'}</NavLink>
      </div>
      {renderLinks()}
    </nav>
  );
}

export default styled(Navigation)(styles);
