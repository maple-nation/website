import React from 'react';
import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s, breakpoints } from 'stylesheet';

const styles = css(s.flex, s.alignCenter, s.justifyCenter, s.wrap, {
  padding: '0 1.25rem',

  li: css(s.flex, s.justifyCenter, {
    width: '50%',
    marginBottom: '1.25rem',

    [breakpoints.desktop]: {
      width: '25%',
    },

    img: {
      margin: '0 auto',
    },
  }),

  ".shrink": {
    maxWidth: '50%',
  },
});

const SponsorList = ({ className }) => {
  return (
    <ul className={className}>
      <li>
        <a href={'https://www.usda.gov/wps/portal/usda/usdahome'}>
          <img className="shrink" src={'/assets/sponsors/usda.jpg'} alt={'USDA'} />
        </a>
      </li>
      <li>
        <a href={'https://www.fs.fed.us/'}>
          <img className="shrink" src={'/assets/sponsors/usfs.jpg'} alt={'USFS'} />
        </a>
      </li>
      <li>
        <a href={'http://www.esf.edu/'}>
          <img className="shrink" src={'/assets/sponsors/esf.png'} alt={'ESF'} />
        </a>
      </li>
      <li>
        <a href={'http://www.esf.edu/nativepeoples/'}>
          <img src={'/assets/sponsors/cnpe.jpg'} alt={'CNPE'} />
        </a>
      </li>
    </ul>
  );
}

export default styled(SponsorList)(styles);
