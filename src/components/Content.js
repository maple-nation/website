import React from 'react';
import ReactMarkdown from 'react-markdown';
import styled from 'react-emotion';
import { copy } from 'stylesheet';

const styles = {
  h1: {
    marginBottom: '1.5625rem',
    ...copy.h2
  },
  p: {
    marginBottom: '1.25rem',
    ...copy.p
  }
};

const Content = (props) => {
  return (
    <div className={props.className}>
      <ReactMarkdown source={props.source} />
    </div>
  );
}

export default styled(Content)(styles);
