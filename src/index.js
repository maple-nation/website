import 'react-app-polyfill/ie9'
import React from 'react';
import { hydrate, render } from 'react-dom';
import promise from 'redux-promise-middleware';
import registerServiceWorker from 'registerServiceWorker';

// state
import { Provider } from 'react-redux';
import {
  connectRouter,
  routerMiddleware,
  ConnectedRouter,
} from "connected-react-router";
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from 'state/reducers';

// routing
import createHistory from 'history/createBrowserHistory';

import ScrollToTop from 'components/ScrollToTop';
import App from 'App';

const history = createHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

// Create Redux store with initial state
const store = createStore(
  connectRouter(history)(reducers),
  preloadedState || {},
  composeEnhancers(
    applyMiddleware(routerMiddleware(history), promise()),
  ),
);

// Tell react-snap how to save Redux state
window.snapSaveState = () => ({
  __PRELOADED_STATE__: store.getState()
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ScrollToTop><App /></ScrollToTop>
    </ConnectedRouter>
  </Provider>
)

// render
const rootElement = document.getElementById("root");

if (rootElement.hasChildNodes()) {
  hydrate(app, rootElement);
} else {
  render(app, rootElement);
}

registerServiceWorker();
