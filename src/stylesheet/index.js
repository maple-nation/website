import { generate } from "cooltilities";
import breakpoints from './breakpoints';
import colors from './colors';
import copy from './copy';

const styles = {
  ...generate(colors),
  ...copy,
}

export { breakpoints, colors, copy, styles };
