import colors from './colors';

const fonts = {
  lato: { fontFamily: "'Lato', sans-serif" },
  oswald: { fontFamily: "'Oswald', sans-serif" },
};

const copy = {
  h1: {
    fontSize: '1.125rem',
    letterSpacing: '0.0625rem',
    textTransform: 'uppercase',
    ...fonts.oswald,
  },
  h2: {
    fontSize: '1.25rem',
    backgroundColor: colors.yellow,
    textTransform: 'uppercase',
    letterSpacing: '0.1875rem',
    padding: '0 1.25rem',
    ...fonts.oswald,
  },
  h3: {
    fontSize: '1.125rem',
    textTransform: 'uppercase',
    ...fonts.oswald,
  },
  h4: {
    fontSize: '1.25rem',
    ...fonts.oswald,
  },
  h5: {
    fontSize: '1rem',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    ...fonts.oswald,
  },
  p: {
    fontSize: '1rem',
    lineHeight: '1.5em',
    ...fonts.lato,
  },
  copyright: {
    fontSize: '0.625rem',
    fontWeight: 'bold',
    ...fonts.lato,
  }
};

export default { ...fonts, ...copy };
