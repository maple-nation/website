export default {
  white: '#ffffff',
  black: '#222222',
  brown: '#4f3717',
  red: '#ed1c24',
  yellow: '#fff200',
};
