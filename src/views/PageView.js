import React from 'react';
import { Link } from 'react-router-dom';
import Video from 'components/Video';
import Content from 'components/Content';
import VideoPlaylist from 'components/VideoPlaylist';
import { css } from 'emotion';
import styled from 'react-emotion';
import { styles as s, breakpoints, copy } from 'stylesheet';

const styles = css(s.flex, s.flexColumn, s.alignCenter, {
  textAlign: 'center',
  padding: '1.25rem',
  fontSize: '1rem',
  lineHeight: '1.5em',
  ...copy.lato,
  [breakpoints.desktop]: {
    padding: '3.125rem',
  },

  '.content': css({
    maginBottom: '1.5625rem',
    ...copy.p,

    [breakpoints.desktop]: {
      width: '65%',
    },

    a: css(s.red, {
      textDecoration: 'none',

      ':hover': css(s.brown),
    }),
  }),

  '.image': {
    marginBottom: '1.5625rem',
  },

  '.list': {
    marginTop: '2rem',
  },

  '.listTitle': css(s.bgYellow, {
    marginBottom: '0.5rem',
    padding: '0 1rem'
  }),

  '.link': {
    marginBottom: '0.5rem',

    a: css(s.red, {
      '@composes': [s.red],
      textDecoration: 'none',
      ':hover': css(s.brown),
    }),
  },
});

const SubpageNavigation = (props) => {
  const { pages } = props;

  const renderSubpages = pages.map(page => {
    const { fields: { name, slug }, sys: { id } } = page;
    return (
      <li key={id} className="link">
        <Link to={`/${slug}`}>{name}</Link>
      </li>
    );
  });

  return (
    <ul className="list">
      <h1 className="listTitle">{'Learn More'}</h1>
      {renderSubpages}
    </ul>
  )
}

const PageView = (props) => {
  const { content, featuredImage, featuredVideo, subpages, videoPlaylist } = props.fields;

  return (
    <div className={props.className}>
      {featuredImage && <img className="image" src={featuredImage.fields.file.url} alt={featuredImage.fields.name} />}
      {featuredVideo && <div><Video {...featuredVideo} /></div>}
      {content && <div className="content"><Content source={content} /></div>}
      {videoPlaylist && <div><VideoPlaylist videos={videoPlaylist} /></div>}
      {subpages && subpages.length > 0 && <SubpageNavigation pages={subpages} />}
    </div>
  );
}

export default styled(PageView)(styles);
